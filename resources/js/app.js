"use strict;"

$(document).ready(function() {
    AppRouter
        .setApp({
            name: "Boilerplate",
            author: "Gobierno Municipal de Nogales",
            url: "https://nogalessonora.gob.mx",
            year: "2018-2021"
        });


    // INITIALIZE APP
    $.get('/admin/defaultState')
        .then(function(r) {
            AppRouter.setDefaultState(r);
            Common.init();
            AppRouter.init();
        });

    $("#change_password").on('click', function() {
        Users.changePassword();
    });
});