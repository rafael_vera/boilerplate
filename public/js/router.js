(function(t,e){"object"==typeof exports&&"undefined"!=typeof module?e(exports):"function"==typeof define&&define.amd?define(["exports"],e):e(t.RSVP={})})(this,function(t){"use strict";function e(t){var e=t._promiseCallbacks;e||(e=t._promiseCallbacks={});return e}function r(t,e){if(2!==arguments.length)return dt[t];dt[t]=e}function n(){setTimeout(function(){for(var t=0;t<vt.length;t++){var e=vt[t],r=e.payload;r.guid=r.key+r.id;r.childGuid=r.key+r.childId;r.error&&(r.stack=r.error.stack);dt.trigger(e.name,e.payload)}vt.length=0},50)}function o(t,e,r){1===vt.push({name:t,payload:{key:e._guidKey,id:e._id,eventName:t,detail:e._result,childId:r&&r._id,label:e._label,timeStamp:Date.now(),error:dt["instrument-with-stack"]?new Error(e._label):null}})&&n()}function i(t,e){var r=this;if(t&&"object"==typeof t&&t.constructor===r)return t;var n=new r(c,e);_(n,t);return n}function s(){return new TypeError("A promises callback cannot return that same promise.")}function u(t){var e=typeof t;return null!==t&&("object"===e||"function"===e)}function c(){}function a(t){try{return t.then}catch(e){gt.error=e;return gt}}function f(){try{var t=jt;jt=null;return t.apply(this,arguments)}catch(e){gt.error=e;return gt}}function l(t){jt=t;return f}function h(t,e,r){dt.async(function(t){var n=!1,o=l(r).call(e,function(r){if(!n){n=!0;e===r?v(t,r):_(t,r)}},function(e){if(!n){n=!0;m(t,e)}},"Settle: "+(t._label||" unknown promise"));if(!n&&o===gt){n=!0;var i=gt.error;gt.error=null;m(t,i)}},t)}function p(t,e){if(e._state===bt)v(t,e._result);else if(e._state===wt){e._onError=null;m(t,e._result)}else b(e,void 0,function(r){e===r?v(t,r):_(t,r)},function(e){return m(t,e)})}function y(t,e,r){var n=e.constructor===t.constructor&&r===O&&t.constructor.resolve===i;if(n)p(t,e);else if(r===gt){var o=gt.error;gt.error=null;m(t,o)}else"function"==typeof r?h(t,e,r):v(t,e)}function _(t,e){t===e?v(t,e):u(e)?y(t,e,a(e)):v(t,e)}function d(t){t._onError&&t._onError(t._result);w(t)}function v(t,e){if(t._state===mt){t._result=e;t._state=bt;0===t._subscribers.length?dt.instrument&&o("fulfilled",t):dt.async(w,t)}}function m(t,e){if(t._state===mt){t._state=wt;t._result=e;dt.async(d,t)}}function b(t,e,r,n){var o=t._subscribers,i=o.length;t._onError=null;o[i]=e;o[i+bt]=r;o[i+wt]=n;0===i&&t._state&&dt.async(w,t)}function w(t){var e=t._subscribers,r=t._state;dt.instrument&&o(r===bt?"fulfilled":"rejected",t);if(0!==e.length){for(var n=void 0,i=void 0,s=t._result,u=0;u<e.length;u+=3){n=e[u];i=e[u+r];n?g(r,n,i,s):i(s)}t._subscribers.length=0}}function g(t,e,r,n){var o="function"==typeof r,i=void 0;i=o?l(r)(n):n;if(e._state!==mt);else if(i===e)m(e,s());else if(i===gt){var u=gt.error;gt.error=null;m(e,u)}else o?_(e,i):t===bt?v(e,i):t===wt&&m(e,i)}function j(t,e){var r=!1;try{e(function(e){if(!r){r=!0;_(t,e)}},function(e){if(!r){r=!0;m(t,e)}})}catch(n){m(t,n)}}function O(t,e,r){var n=this,i=n._state;if(i===bt&&!t||i===wt&&!e){dt.instrument&&o("chained",n,n);return n}n._onError=null;var s=new n.constructor(c,r),u=n._result;dt.instrument&&o("chained",n,s);if(i===mt)b(n,s,t,e);else{var a=i===bt?t:e;dt.async(function(){return g(i,s,a,u)})}return s}function A(t,e,r){this._remaining--;t===bt?this._result[e]={state:"fulfilled",value:r}:this._result[e]={state:"rejected",reason:r}}function E(t,e){return Array.isArray(t)?new Ot(this,t,(!0),e).promise:this.reject(new TypeError("Promise.all must be called with an array"),e)}function T(t,e){var r=this,n=new r(c,e);if(!Array.isArray(t)){m(n,new TypeError("Promise.race must be called with an array"));return n}for(var o=0;n._state===mt&&o<t.length;o++)b(r.resolve(t[o]),void 0,function(t){return _(n,t)},function(t){return m(n,t)});return n}function P(t,e){var r=this,n=new r(c,e);m(n,t);return n}function S(){throw new TypeError("You must pass a resolver function as the first argument to the promise constructor")}function R(){throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.")}function x(t,e){for(var r={},n=t.length,o=new Array(n),i=0;i<n;i++)o[i]=t[i];for(var s=0;s<e.length;s++){var u=e[s];r[u]=o[s+1]}return r}function k(t){for(var e=t.length,r=new Array(e-1),n=1;n<e;n++)r[n-1]=t[n];return r}function M(t,e){return{then:function(r,n){return t.call(e,r,n)}}}function C(t,e){var r=function(){for(var r=arguments.length,n=new Array(r+1),o=!1,i=0;i<r;++i){var s=arguments[i];if(!o){o=N(s);if(o===gt){var u=gt.error;gt.error=null;var a=new Tt(c);m(a,u);return a}o&&o!==!0&&(s=M(o,s))}n[i]=s}var f=new Tt(c);n[r]=function(t,r){t?m(f,t):void 0===e?_(f,r):e===!0?_(f,k(arguments)):Array.isArray(e)?_(f,x(arguments,e)):_(f,r)};return o?I(f,n,t,this):F(f,n,t,this)};r.__proto__=t;return r}function F(t,e,r,n){var o=l(r).apply(n,e);if(o===gt){var i=gt.error;gt.error=null;m(t,i)}return t}function I(t,e,r,n){return Tt.all(e).then(function(e){return F(t,e,r,n)})}function N(t){return null!==t&&"object"==typeof t&&(t.constructor===Tt||a(t))}function U(t,e){return Tt.all(t,e)}function V(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}function D(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}});e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}function K(t,e){return Array.isArray(t)?new Pt(Tt,t,e).promise:Tt.reject(new TypeError("Promise.allSettled must be called with an array"),e)}function q(t,e){return Tt.race(t,e)}function G(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}function L(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}});e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}function W(t,e){return null===t||"object"!=typeof t?Tt.reject(new TypeError("Promise.hash must be called with an object"),e):new Rt(Tt,t,e).promise}function Y(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}function $(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}});e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}function z(t,e){return null===t||"object"!=typeof t?Tt.reject(new TypeError("RSVP.hashSettled must be called with an object"),e):new xt(Tt,t,(!1),e).promise}function B(t){setTimeout(function(){throw t});throw t}function H(t){var e={resolve:void 0,reject:void 0};e.promise=new Tt(function(t,r){e.resolve=t;e.reject=r},t);return e}function J(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}function Q(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}});e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}function X(t,e,r){return Array.isArray(t)?"function"!=typeof e?Tt.reject(new TypeError("RSVP.map expects a function as a second argument"),r):new kt(Tt,t,e,r).promise:Tt.reject(new TypeError("RSVP.map must be called with an array"),r)}function Z(t,e){return Tt.resolve(t,e)}function tt(t,e){return Tt.reject(t,e)}function et(t,e){if(!t)throw new ReferenceError("this hasn't been initialised - super() hasn't been called");return!e||"object"!=typeof e&&"function"!=typeof e?t:e}function rt(t,e){if("function"!=typeof e&&null!==e)throw new TypeError("Super expression must either be null or a function, not "+typeof e);t.prototype=Object.create(e&&e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}});e&&(Object.setPrototypeOf?Object.setPrototypeOf(t,e):t.__proto__=e)}function nt(t,e,r){return"function"!=typeof e?Tt.reject(new TypeError("RSVP.filter expects function as a second argument"),r):Tt.resolve(t,r).then(function(t){if(!Array.isArray(t))throw new TypeError("RSVP.filter must be called with an array");return new Ct(Tt,t,e,r).promise})}function ot(t,e){qt[Ft]=t;qt[Ft+1]=e;Ft+=2;2===Ft&&Gt()}function it(){var t=process.nextTick,e=process.versions.node.match(/^(?:(\d+)\.)?(?:(\d+)\.)?(\*|\d+)$/);Array.isArray(e)&&"0"===e[1]&&"10"===e[2]&&(t=setImmediate);return function(){return t(ft)}}function st(){return"undefined"!=typeof It?function(){It(ft)}:at()}function ut(){var t=0,e=new Vt(ft),r=document.createTextNode("");e.observe(r,{characterData:!0});return function(){return r.data=t=++t%2}}function ct(){var t=new MessageChannel;t.port1.onmessage=ft;return function(){return t.port2.postMessage(0)}}function at(){return function(){return setTimeout(ft,1)}}function ft(){for(var t=0;t<Ft;t+=2){var e=qt[t],r=qt[t+1];e(r);qt[t]=void 0;qt[t+1]=void 0}Ft=0}function lt(){try{var t=require,e=t("vertx");It=e.runOnLoop||e.runOnContext;return st()}catch(r){return at()}}function ht(t,e,r){e in t?Object.defineProperty(t,e,{value:r,enumerable:!0,configurable:!0,writable:!0}):t[e]=r;return t}function pt(){dt.on.apply(dt,arguments)}function yt(){dt.off.apply(dt,arguments)}var _t={mixin:function(t){t.on=this.on;t.off=this.off;t.trigger=this.trigger;t._promiseCallbacks=void 0;return t},on:function(t,r){if("function"!=typeof r)throw new TypeError("Callback must be a function");var n=e(this),o=void 0;o=n[t];o||(o=n[t]=[]);o.indexOf(r)&&o.push(r)},off:function(t,r){var n=e(this),o=void 0,i=void 0;if(r){o=n[t];i=o.indexOf(r);i!==-1&&o.splice(i,1)}else n[t]=[]},trigger:function(t,r,n){var o=e(this),i=void 0,s=void 0;if(i=o[t])for(var u=0;u<i.length;u++){s=i[u];s(r,n)}}},dt={instrument:!1};_t.mixin(dt);var vt=[],mt=void 0,bt=1,wt=2,gt={error:null},jt=void 0,Ot=function(){function t(t,e,r,n){this._instanceConstructor=t;this.promise=new t(c,n);this._abortOnReject=r;this._isUsingOwnPromise=t===Tt;this._isUsingOwnResolve=t.resolve===i;this._init.apply(this,arguments)}t.prototype._init=function(t,e){var r=e.length||0;this.length=r;this._remaining=r;this._result=new Array(r);this._enumerate(e)};t.prototype._enumerate=function(t){for(var e=this.length,r=this.promise,n=0;r._state===mt&&n<e;n++)this._eachEntry(t[n],n,!0);this._checkFullfillment()};t.prototype._checkFullfillment=function(){0===this._remaining&&v(this.promise,this._result)};t.prototype._settleMaybeThenable=function(t,e,r){var n=this._instanceConstructor;if(this._isUsingOwnResolve){var o=a(t);if(o===O&&t._state!==mt){t._onError=null;this._settledAt(t._state,e,t._result,r)}else if("function"!=typeof o)this._settledAt(bt,e,t,r);else if(this._isUsingOwnPromise){var i=new n(c);y(i,t,o);this._willSettleAt(i,e,r)}else this._willSettleAt(new n(function(e){return e(t)}),e,r)}else this._willSettleAt(n.resolve(t),e,r)};t.prototype._eachEntry=function(t,e,r){null!==t&&"object"==typeof t?this._settleMaybeThenable(t,e,r):this._setResultAt(bt,e,t,r)};t.prototype._settledAt=function(t,e,r,n){var o=this.promise;if(o._state===mt)if(this._abortOnReject&&t===wt)m(o,r);else{this._setResultAt(t,e,r,n);this._checkFullfillment()}};t.prototype._setResultAt=function(t,e,r,n){this._remaining--;this._result[e]=r};t.prototype._willSettleAt=function(t,e,r){var n=this;b(t,void 0,function(t){return n._settledAt(bt,e,t,r)},function(t){return n._settledAt(wt,e,t,r)})};return t}(),At="rsvp_"+Date.now()+"-",Et=0,Tt=function(){function t(e,r){this._id=Et++;this._label=r;this._state=void 0;this._result=void 0;this._subscribers=[];dt.instrument&&o("created",this);if(c!==e){"function"!=typeof e&&S();this instanceof t?j(this,e):R()}}t.prototype._onError=function(t){var e=this;dt.after(function(){e._onError&&dt.trigger("error",t,e._label)})};t.prototype["catch"]=function(t,e){return this.then(void 0,t,e)};t.prototype["finally"]=function(t,e){var r=this,n=r.constructor;return r.then(function(e){return n.resolve(t()).then(function(){return e})},function(e){return n.resolve(t()).then(function(){throw e})},e)};return t}();Tt.all=E;Tt.race=T;Tt.resolve=i;Tt.reject=P;Tt.prototype._guidKey=At;Tt.prototype.then=O;var Pt=function(t){function e(e,r,n){return V(this,t.call(this,e,r,!1,n))}D(e,t);return e}(Ot);Pt.prototype._setResultAt=A;var St=Object.prototype.hasOwnProperty,Rt=function(t){function e(e,r){var n=!(arguments.length>2&&void 0!==arguments[2])||arguments[2],o=arguments[3];return G(this,t.call(this,e,r,n,o))}L(e,t);e.prototype._init=function(t,e){this._result={};this._enumerate(e);0===this._remaining&&v(this.promise,this._result)};e.prototype._enumerate=function(t){var e=this.promise,r=[];for(var n in t)St.call(t,n)&&r.push({position:n,entry:t[n]});var o=r.length;this._remaining=o;for(var i=void 0,s=0;e._state===mt&&s<o;s++){i=r[s];this._eachEntry(i.entry,i.position)}};return e}(Ot),xt=function(t){function e(e,r,n){return Y(this,t.call(this,e,r,!1,n))}$(e,t);return e}(Rt);xt.prototype._setResultAt=A;var kt=function(t){function e(e,r,n,o){return J(this,t.call(this,e,r,!0,o,n))}Q(e,t);e.prototype._init=function(t,e,r,n,o){var i=e.length||0;this.length=i;this._remaining=i;this._result=new Array(i);this._mapFn=o;this._enumerate(e)};e.prototype._setResultAt=function(t,e,r,n){if(n){var o=l(this._mapFn)(r,e);o===gt?this._settledAt(wt,e,o.error,!1):this._eachEntry(o,e,!1)}else{this._remaining--;this._result[e]=r}};return e}(Ot),Mt={},Ct=function(t){function e(e,r,n,o){return et(this,t.call(this,e,r,!0,o,n))}rt(e,t);e.prototype._init=function(t,e,r,n,o){var i=e.length||0;this.length=i;this._remaining=i;this._result=new Array(i);this._filterFn=o;this._enumerate(e)};e.prototype._checkFullfillment=function(){if(0===this._remaining){this._result=this._result.filter(function(t){return t!==Mt});v(this.promise,this._result)}};e.prototype._setResultAt=function(t,e,r,n){if(n){this._result[e]=r;var o=l(this._filterFn)(r,e);o===gt?this._settledAt(wt,e,o.error,!1):this._eachEntry(o,e,!1)}else{this._remaining--;r||(this._result[e]=Mt)}};return e}(Ot),Ft=0,It=void 0,Nt="undefined"!=typeof window?window:void 0,Ut=Nt||{},Vt=Ut.MutationObserver||Ut.WebKitMutationObserver,Dt="undefined"==typeof self&&"undefined"!=typeof process&&"[object process]"==={}.toString.call(process),Kt="undefined"!=typeof Uint8ClampedArray&&"undefined"!=typeof importScripts&&"undefined"!=typeof MessageChannel,qt=new Array(1e3),Gt=void 0;Gt=Dt?it():Vt?ut():Kt?ct():void 0===Nt&&"function"==typeof require?lt():at();var Lt;dt.async=ot;dt.after=function(t){return setTimeout(t,0)};var Wt=function(t,e){return dt.async(t,e)};if("undefined"!=typeof window&&"object"==typeof window.__PROMISE_INSTRUMENTATION__){var Yt=window.__PROMISE_INSTRUMENTATION__;r("instrument",!0);for(var $t in Yt)Yt.hasOwnProperty($t)&&pt($t,Yt[$t])}var zt=(Lt={asap:ot,Promise:Tt,EventTarget:_t,all:U,allSettled:K,race:q,hash:W,hashSettled:z,rethrow:B,defer:H,denodeify:C,configure:r,on:pt,off:yt,resolve:Z,reject:tt,map:X},ht(Lt,"async",Wt),ht(Lt,"filter",nt),Lt);t["default"]=zt;t.asap=ot;t.Promise=Tt;t.EventTarget=_t;t.all=U;t.allSettled=K;t.race=q;t.hash=W;t.hashSettled=z;t.rethrow=B;t.defer=H;t.denodeify=C;t.configure=r;t.on=pt;t.off=yt;t.resolve=Z;t.reject=tt;t.map=X;t.async=Wt;t.filter=nt;Object.defineProperty(t,"__esModule",{value:!0})});
/*

 JS Signals <http://millermedeiros.github.com/js-signals/>
 Released under the MIT license
 Author: Miller Medeiros
 Version: 1.0.0 - Build: 268 (2012/11/29 05:48 PM)
*/
(function(i){function h(a,b,c,d,e){this._listener=b;this._isOnce=c;this.context=d;this._signal=a;this._priority=e||0}function g(a,b){if(typeof a!=="function")throw Error("listener is a required param of {fn}() and should be a Function.".replace("{fn}",b));}function e(){this._bindings=[];this._prevParams=null;var a=this;this.dispatch=function(){e.prototype.dispatch.apply(a,arguments)}}h.prototype={active:!0,params:null,execute:function(a){var b;this.active&&this._listener&&(a=this.params?this.params.concat(a):
a,b=this._listener.apply(this.context,a),this._isOnce&&this.detach());return b},detach:function(){return this.isBound()?this._signal.remove(this._listener,this.context):null},isBound:function(){return!!this._signal&&!!this._listener},isOnce:function(){return this._isOnce},getListener:function(){return this._listener},getSignal:function(){return this._signal},_destroy:function(){delete this._signal;delete this._listener;delete this.context},toString:function(){return"[SignalBinding isOnce:"+this._isOnce+
", isBound:"+this.isBound()+", active:"+this.active+"]"}};e.prototype={VERSION:"1.0.0",memorize:!1,_shouldPropagate:!0,active:!0,_registerListener:function(a,b,c,d){var e=this._indexOfListener(a,c);if(e!==-1){if(a=this._bindings[e],a.isOnce()!==b)throw Error("You cannot add"+(b?"":"Once")+"() then add"+(!b?"":"Once")+"() the same listener without removing the relationship first.");}else a=new h(this,a,b,c,d),this._addBinding(a);this.memorize&&this._prevParams&&a.execute(this._prevParams);return a},
_addBinding:function(a){var b=this._bindings.length;do--b;while(this._bindings[b]&&a._priority<=this._bindings[b]._priority);this._bindings.splice(b+1,0,a)},_indexOfListener:function(a,b){for(var c=this._bindings.length,d;c--;)if(d=this._bindings[c],d._listener===a&&d.context===b)return c;return-1},has:function(a,b){return this._indexOfListener(a,b)!==-1},add:function(a,b,c){g(a,"add");return this._registerListener(a,!1,b,c)},addOnce:function(a,b,c){g(a,"addOnce");return this._registerListener(a,
!0,b,c)},remove:function(a,b){g(a,"remove");var c=this._indexOfListener(a,b);c!==-1&&(this._bindings[c]._destroy(),this._bindings.splice(c,1));return a},removeAll:function(){for(var a=this._bindings.length;a--;)this._bindings[a]._destroy();this._bindings.length=0},getNumListeners:function(){return this._bindings.length},halt:function(){this._shouldPropagate=!1},dispatch:function(a){if(this.active){var b=Array.prototype.slice.call(arguments),c=this._bindings.length,d;if(this.memorize)this._prevParams=
b;if(c){d=this._bindings.slice();this._shouldPropagate=!0;do c--;while(d[c]&&this._shouldPropagate&&d[c].execute(b)!==!1)}}},forget:function(){this._prevParams=null},dispose:function(){this.removeAll();delete this._bindings;delete this._prevParams},toString:function(){return"[Signal active:"+this.active+" numListeners:"+this.getNumListeners()+"]"}};var f=e;f.Signal=e;typeof define==="function"&&define.amd?define(function(){return f}):typeof module!=="undefined"&&module.exports?module.exports=f:i.signals=
f})(this);
(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
	typeof define === 'function' && define.amd ? define('route-recognizer', factory) :
	(global.RouteRecognizer = factory());
}(this, (function () { 'use strict';

var createObject = Object.create;
function createMap() {
    var map = createObject(null);
    map["__"] = undefined;
    delete map["__"];
    return map;
}

var Target = function Target(path, matcher, delegate) {
    this.path = path;
    this.matcher = matcher;
    this.delegate = delegate;
};
Target.prototype.to = function to (target, callback) {
    var delegate = this.delegate;
    if (delegate && delegate.willAddRoute) {
        target = delegate.willAddRoute(this.matcher.target, target);
    }
    this.matcher.add(this.path, target);
    if (callback) {
        if (callback.length === 0) {
            throw new Error("You must have an argument in the function passed to `to`");
        }
        this.matcher.addChild(this.path, target, callback, this.delegate);
    }
};
var Matcher = function Matcher(target) {
    this.routes = createMap();
    this.children = createMap();
    this.target = target;
};
Matcher.prototype.add = function add (path, target) {
    this.routes[path] = target;
};
Matcher.prototype.addChild = function addChild (path, target, callback, delegate) {
    var matcher = new Matcher(target);
    this.children[path] = matcher;
    var match = generateMatch(path, matcher, delegate);
    if (delegate && delegate.contextEntered) {
        delegate.contextEntered(target, match);
    }
    callback(match);
};
function generateMatch(startingPath, matcher, delegate) {
    function match(path, callback) {
        var fullPath = startingPath + path;
        if (callback) {
            callback(generateMatch(fullPath, matcher, delegate));
        }
        else {
            return new Target(fullPath, matcher, delegate);
        }
    }
    
    return match;
}
function addRoute(routeArray, path, handler) {
    var len = 0;
    for (var i = 0; i < routeArray.length; i++) {
        len += routeArray[i].path.length;
    }
    path = path.substr(len);
    var route = { path: path, handler: handler };
    routeArray.push(route);
}
function eachRoute(baseRoute, matcher, callback, binding) {
    var routes = matcher.routes;
    var paths = Object.keys(routes);
    for (var i = 0; i < paths.length; i++) {
        var path = paths[i];
        var routeArray = baseRoute.slice();
        addRoute(routeArray, path, routes[path]);
        var nested = matcher.children[path];
        if (nested) {
            eachRoute(routeArray, nested, callback, binding);
        }
        else {
            callback.call(binding, routeArray);
        }
    }
}
var map = function (callback, addRouteCallback) {
    var matcher = new Matcher();
    callback(generateMatch("", matcher, this.delegate));
    eachRoute([], matcher, function (routes) {
        if (addRouteCallback) {
            addRouteCallback(this, routes);
        }
        else {
            this.add(routes);
        }
    }, this);
};

// Normalizes percent-encoded values in `path` to upper-case and decodes percent-encoded
// values that are not reserved (i.e., unicode characters, emoji, etc). The reserved
// chars are "/" and "%".
// Safe to call multiple times on the same path.
// Normalizes percent-encoded values in `path` to upper-case and decodes percent-encoded
function normalizePath(path) {
    return path.split("/")
        .map(normalizeSegment)
        .join("/");
}
// We want to ensure the characters "%" and "/" remain in percent-encoded
// form when normalizing paths, so replace them with their encoded form after
// decoding the rest of the path
var SEGMENT_RESERVED_CHARS = /%|\//g;
function normalizeSegment(segment) {
    if (segment.length < 3 || segment.indexOf("%") === -1)
        { return segment; }
    return decodeURIComponent(segment).replace(SEGMENT_RESERVED_CHARS, encodeURIComponent);
}
// We do not want to encode these characters when generating dynamic path segments
// See https://tools.ietf.org/html/rfc3986#section-3.3
// sub-delims: "!", "$", "&", "'", "(", ")", "*", "+", ",", ";", "="
// others allowed by RFC 3986: ":", "@"
//
// First encode the entire path segment, then decode any of the encoded special chars.
//
// The chars "!", "'", "(", ")", "*" do not get changed by `encodeURIComponent`,
// so the possible encoded chars are:
// ['%24', '%26', '%2B', '%2C', '%3B', '%3D', '%3A', '%40'].
var PATH_SEGMENT_ENCODINGS = /%(?:2(?:4|6|B|C)|3(?:B|D|A)|40)/g;
function encodePathSegment(str) {
    return encodeURIComponent(str).replace(PATH_SEGMENT_ENCODINGS, decodeURIComponent);
}

var escapeRegex = /(\/|\.|\*|\+|\?|\||\(|\)|\[|\]|\{|\}|\\)/g;
var isArray = Array.isArray;
var hasOwnProperty = Object.prototype.hasOwnProperty;
function getParam(params, key) {
    if (typeof params !== "object" || params === null) {
        throw new Error("You must pass an object as the second argument to `generate`.");
    }
    if (!hasOwnProperty.call(params, key)) {
        throw new Error("You must provide param `" + key + "` to `generate`.");
    }
    var value = params[key];
    var str = typeof value === "string" ? value : "" + value;
    if (str.length === 0) {
        throw new Error("You must provide a param `" + key + "`.");
    }
    return str;
}
var eachChar = [];
eachChar[0 /* Static */] = function (segment, currentState) {
    var state = currentState;
    var value = segment.value;
    for (var i = 0; i < value.length; i++) {
        var ch = value.charCodeAt(i);
        state = state.put(ch, false, false);
    }
    return state;
};
eachChar[1 /* Dynamic */] = function (_, currentState) {
    return currentState.put(47 /* SLASH */, true, true);
};
eachChar[2 /* Star */] = function (_, currentState) {
    return currentState.put(-1 /* ANY */, false, true);
};
eachChar[4 /* Epsilon */] = function (_, currentState) {
    return currentState;
};
var regex = [];
regex[0 /* Static */] = function (segment) {
    return segment.value.replace(escapeRegex, "\\$1");
};
regex[1 /* Dynamic */] = function () {
    return "([^/]+)";
};
regex[2 /* Star */] = function () {
    return "(.+)";
};
regex[4 /* Epsilon */] = function () {
    return "";
};
var generate = [];
generate[0 /* Static */] = function (segment) {
    return segment.value;
};
generate[1 /* Dynamic */] = function (segment, params) {
    var value = getParam(params, segment.value);
    if (RouteRecognizer.ENCODE_AND_DECODE_PATH_SEGMENTS) {
        return encodePathSegment(value);
    }
    else {
        return value;
    }
};
generate[2 /* Star */] = function (segment, params) {
    return getParam(params, segment.value);
};
generate[4 /* Epsilon */] = function () {
    return "";
};
var EmptyObject = Object.freeze({});
var EmptyArray = Object.freeze([]);
// The `names` will be populated with the paramter name for each dynamic/star
// segment. `shouldDecodes` will be populated with a boolean for each dyanamic/star
// segment, indicating whether it should be decoded during recognition.
function parse(segments, route, types) {
    // normalize route as not starting with a "/". Recognition will
    // also normalize.
    if (route.length > 0 && route.charCodeAt(0) === 47 /* SLASH */) {
        route = route.substr(1);
    }
    var parts = route.split("/");
    var names = undefined;
    var shouldDecodes = undefined;
    for (var i = 0; i < parts.length; i++) {
        var part = parts[i];
        var flags = 0;
        var type = 0;
        if (part === "") {
            type = 4 /* Epsilon */;
        }
        else if (part.charCodeAt(0) === 58 /* COLON */) {
            type = 1 /* Dynamic */;
        }
        else if (part.charCodeAt(0) === 42 /* STAR */) {
            type = 2 /* Star */;
        }
        else {
            type = 0 /* Static */;
        }
        flags = 2 << type;
        if (flags & 12 /* Named */) {
            part = part.slice(1);
            names = names || [];
            names.push(part);
            shouldDecodes = shouldDecodes || [];
            shouldDecodes.push((flags & 4 /* Decoded */) !== 0);
        }
        if (flags & 14 /* Counted */) {
            types[type]++;
        }
        segments.push({
            type: type,
            value: normalizeSegment(part)
        });
    }
    return {
        names: names || EmptyArray,
        shouldDecodes: shouldDecodes || EmptyArray,
    };
}
function isEqualCharSpec(spec, char, negate) {
    return spec.char === char && spec.negate === negate;
}
// A State has a character specification and (`charSpec`) and a list of possible
// subsequent states (`nextStates`).
//
// If a State is an accepting state, it will also have several additional
// properties:
//
// * `regex`: A regular expression that is used to extract parameters from paths
//   that reached this accepting state.
// * `handlers`: Information on how to convert the list of captures into calls
//   to registered handlers with the specified parameters
// * `types`: How many static, dynamic or star segments in this route. Used to
//   decide which route to use if multiple registered routes match a path.
//
// Currently, State is implemented naively by looping over `nextStates` and
// comparing a character specification against a character. A more efficient
// implementation would use a hash of keys pointing at one or more next states.
var State = function State(states, id, char, negate, repeat) {
    this.states = states;
    this.id = id;
    this.char = char;
    this.negate = negate;
    this.nextStates = repeat ? id : null;
    this.pattern = "";
    this._regex = undefined;
    this.handlers = undefined;
    this.types = undefined;
};
State.prototype.regex = function regex$1 () {
    if (!this._regex) {
        this._regex = new RegExp(this.pattern);
    }
    return this._regex;
};
State.prototype.get = function get (char, negate) {
        var this$1 = this;

    var nextStates = this.nextStates;
    if (nextStates === null)
        { return; }
    if (isArray(nextStates)) {
        for (var i = 0; i < nextStates.length; i++) {
            var child = this$1.states[nextStates[i]];
            if (isEqualCharSpec(child, char, negate)) {
                return child;
            }
        }
    }
    else {
        var child$1 = this.states[nextStates];
        if (isEqualCharSpec(child$1, char, negate)) {
            return child$1;
        }
    }
};
State.prototype.put = function put (char, negate, repeat) {
    var state;
    // If the character specification already exists in a child of the current
    // state, just return that state.
    if (state = this.get(char, negate)) {
        return state;
    }
    // Make a new state for the character spec
    var states = this.states;
    state = new State(states, states.length, char, negate, repeat);
    states[states.length] = state;
    // Insert the new state as a child of the current state
    if (this.nextStates == null) {
        this.nextStates = state.id;
    }
    else if (isArray(this.nextStates)) {
        this.nextStates.push(state.id);
    }
    else {
        this.nextStates = [this.nextStates, state.id];
    }
    // Return the new state
    return state;
};
// Find a list of child states matching the next character
State.prototype.match = function match (ch) {
        var this$1 = this;

    var nextStates = this.nextStates;
    if (!nextStates)
        { return []; }
    var returned = [];
    if (isArray(nextStates)) {
        for (var i = 0; i < nextStates.length; i++) {
            var child = this$1.states[nextStates[i]];
            if (isMatch(child, ch)) {
                returned.push(child);
            }
        }
    }
    else {
        var child$1 = this.states[nextStates];
        if (isMatch(child$1, ch)) {
            returned.push(child$1);
        }
    }
    return returned;
};
function isMatch(spec, char) {
    return spec.negate ? spec.char !== char && spec.char !== -1 /* ANY */ : spec.char === char || spec.char === -1 /* ANY */;
}
// This is a somewhat naive strategy, but should work in a lot of cases
// A better strategy would properly resolve /posts/:id/new and /posts/edit/:id.
//
// This strategy generally prefers more static and less dynamic matching.
// Specifically, it
//
//  * prefers fewer stars to more, then
//  * prefers using stars for less of the match to more, then
//  * prefers fewer dynamic segments to more, then
//  * prefers more static segments to more
function sortSolutions(states) {
    return states.sort(function (a, b) {
        var ref = a.types || [0, 0, 0];
        var astatics = ref[0];
        var adynamics = ref[1];
        var astars = ref[2];
        var ref$1 = b.types || [0, 0, 0];
        var bstatics = ref$1[0];
        var bdynamics = ref$1[1];
        var bstars = ref$1[2];
        if (astars !== bstars) {
            return astars - bstars;
        }
        if (astars) {
            if (astatics !== bstatics) {
                return bstatics - astatics;
            }
            if (adynamics !== bdynamics) {
                return bdynamics - adynamics;
            }
        }
        if (adynamics !== bdynamics) {
            return adynamics - bdynamics;
        }
        if (astatics !== bstatics) {
            return bstatics - astatics;
        }
        return 0;
    });
}
function recognizeChar(states, ch) {
    var nextStates = [];
    for (var i = 0, l = states.length; i < l; i++) {
        var state = states[i];
        nextStates = nextStates.concat(state.match(ch));
    }
    return nextStates;
}
var RecognizeResults = function RecognizeResults(queryParams) {
    this.length = 0;
    this.queryParams = queryParams || {};
};

RecognizeResults.prototype.splice = Array.prototype.splice;
RecognizeResults.prototype.slice = Array.prototype.slice;
RecognizeResults.prototype.push = Array.prototype.push;
function findHandler(state, originalPath, queryParams) {
    var handlers = state.handlers;
    var regex = state.regex();
    if (!regex || !handlers)
        { throw new Error("state not initialized"); }
    var captures = originalPath.match(regex);
    var currentCapture = 1;
    var result = new RecognizeResults(queryParams);
    result.length = handlers.length;
    for (var i = 0; i < handlers.length; i++) {
        var handler = handlers[i];
        var names = handler.names;
        var shouldDecodes = handler.shouldDecodes;
        var params = EmptyObject;
        var isDynamic = false;
        if (names !== EmptyArray && shouldDecodes !== EmptyArray) {
            for (var j = 0; j < names.length; j++) {
                isDynamic = true;
                var name = names[j];
                var capture = captures && captures[currentCapture++];
                if (params === EmptyObject) {
                    params = {};
                }
                if (RouteRecognizer.ENCODE_AND_DECODE_PATH_SEGMENTS && shouldDecodes[j]) {
                    params[name] = capture && decodeURIComponent(capture);
                }
                else {
                    params[name] = capture;
                }
            }
        }
        result[i] = {
            handler: handler.handler,
            params: params,
            isDynamic: isDynamic
        };
    }
    return result;
}
function decodeQueryParamPart(part) {
    // http://www.w3.org/TR/html401/interact/forms.html#h-17.13.4.1
    part = part.replace(/\+/gm, "%20");
    var result;
    try {
        result = decodeURIComponent(part);
    }
    catch (error) {
        result = "";
    }
    return result;
}
var RouteRecognizer = function RouteRecognizer() {
    this.names = createMap();
    var states = [];
    var state = new State(states, 0, -1 /* ANY */, true, false);
    states[0] = state;
    this.states = states;
    this.rootState = state;
};
RouteRecognizer.prototype.add = function add (routes, options) {
    var currentState = this.rootState;
    var pattern = "^";
    var types = [0, 0, 0];
    var handlers = new Array(routes.length);
    var allSegments = [];
    var isEmpty = true;
    var j = 0;
    for (var i = 0; i < routes.length; i++) {
        var route = routes[i];
        var ref = parse(allSegments, route.path, types);
            var names = ref.names;
            var shouldDecodes = ref.shouldDecodes;
        // preserve j so it points to the start of newly added segments
        for (; j < allSegments.length; j++) {
            var segment = allSegments[j];
            if (segment.type === 4 /* Epsilon */) {
                continue;
            }
            isEmpty = false;
            // Add a "/" for the new segment
            currentState = currentState.put(47 /* SLASH */, false, false);
            pattern += "/";
            // Add a representation of the segment to the NFA and regex
            currentState = eachChar[segment.type](segment, currentState);
            pattern += regex[segment.type](segment);
        }
        handlers[i] = {
            handler: route.handler,
            names: names,
            shouldDecodes: shouldDecodes
        };
    }
    if (isEmpty) {
        currentState = currentState.put(47 /* SLASH */, false, false);
        pattern += "/";
    }
    currentState.handlers = handlers;
    currentState.pattern = pattern + "$";
    currentState.types = types;
    var name;
    if (typeof options === "object" && options !== null && options.as) {
        name = options.as;
    }
    if (name) {
        // if (this.names[name]) {
        //   throw new Error("You may not add a duplicate route named `" + name + "`.");
        // }
        this.names[name] = {
            segments: allSegments,
            handlers: handlers
        };
    }
};
RouteRecognizer.prototype.handlersFor = function handlersFor (name) {
    var route = this.names[name];
    if (!route) {
        throw new Error("There is no route named " + name);
    }
    var result = new Array(route.handlers.length);
    for (var i = 0; i < route.handlers.length; i++) {
        var handler = route.handlers[i];
        result[i] = handler;
    }
    return result;
};
RouteRecognizer.prototype.hasRoute = function hasRoute (name) {
    return !!this.names[name];
};
RouteRecognizer.prototype.generate = function generate$1 (name, params) {
    var route = this.names[name];
    var output = "";
    if (!route) {
        throw new Error("There is no route named " + name);
    }
    var segments = route.segments;
    for (var i = 0; i < segments.length; i++) {
        var segment = segments[i];
        if (segment.type === 4 /* Epsilon */) {
            continue;
        }
        output += "/";
        output += generate[segment.type](segment, params);
    }
    if (output.charAt(0) !== "/") {
        output = "/" + output;
    }
    if (params && params.queryParams) {
        output += this.generateQueryString(params.queryParams);
    }
    return output;
};
RouteRecognizer.prototype.generateQueryString = function generateQueryString (params) {
    var pairs = [];
    var keys = Object.keys(params);
    keys.sort();
    for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        var value = params[key];
        if (value == null) {
            continue;
        }
        var pair = encodeURIComponent(key);
        if (isArray(value)) {
            for (var j = 0; j < value.length; j++) {
                var arrayPair = key + "[]" + "=" + encodeURIComponent(value[j]);
                pairs.push(arrayPair);
            }
        }
        else {
            pair += "=" + encodeURIComponent(value);
            pairs.push(pair);
        }
    }
    if (pairs.length === 0) {
        return "";
    }
    return "?" + pairs.join("&");
};
RouteRecognizer.prototype.parseQueryString = function parseQueryString (queryString) {
    var pairs = queryString.split("&");
    var queryParams = {};
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split("="), key = decodeQueryParamPart(pair[0]), keyLength = key.length, isArray = false, value = (void 0);
        if (pair.length === 1) {
            value = "true";
        }
        else {
            // Handle arrays
            if (keyLength > 2 && key.slice(keyLength - 2) === "[]") {
                isArray = true;
                key = key.slice(0, keyLength - 2);
                if (!queryParams[key]) {
                    queryParams[key] = [];
                }
            }
            value = pair[1] ? decodeQueryParamPart(pair[1]) : "";
        }
        if (isArray) {
            queryParams[key].push(value);
        }
        else {
            queryParams[key] = value;
        }
    }
    return queryParams;
};
RouteRecognizer.prototype.recognize = function recognize (path) {
    var results;
    var states = [this.rootState];
    var queryParams = {};
    var isSlashDropped = false;
    var hashStart = path.indexOf("#");
    if (hashStart !== -1) {
        path = path.substr(0, hashStart);
    }
    var queryStart = path.indexOf("?");
    if (queryStart !== -1) {
        var queryString = path.substr(queryStart + 1, path.length);
        path = path.substr(0, queryStart);
        queryParams = this.parseQueryString(queryString);
    }
    if (path.charAt(0) !== "/") {
        path = "/" + path;
    }
    var originalPath = path;
    if (RouteRecognizer.ENCODE_AND_DECODE_PATH_SEGMENTS) {
        path = normalizePath(path);
    }
    else {
        path = decodeURI(path);
        originalPath = decodeURI(originalPath);
    }
    var pathLen = path.length;
    if (pathLen > 1 && path.charAt(pathLen - 1) === "/") {
        path = path.substr(0, pathLen - 1);
        originalPath = originalPath.substr(0, originalPath.length - 1);
        isSlashDropped = true;
    }
    for (var i = 0; i < path.length; i++) {
        states = recognizeChar(states, path.charCodeAt(i));
        if (!states.length) {
            break;
        }
    }
    var solutions = [];
    for (var i$1 = 0; i$1 < states.length; i$1++) {
        if (states[i$1].handlers) {
            solutions.push(states[i$1]);
        }
    }
    states = sortSolutions(solutions);
    var state = solutions[0];
    if (state && state.handlers) {
        // if a trailing slash was dropped and a star segment is the last segment
        // specified, put the trailing slash back
        if (isSlashDropped && state.pattern && state.pattern.slice(-5) === "(.+)$") {
            originalPath = originalPath + "/";
        }
        results = findHandler(state, originalPath, queryParams);
    }
    return results;
};
RouteRecognizer.VERSION = "0.3.4";
// Set to false to opt-out of encoding and decoding path segments.
// See https://github.com/tildeio/route-recognizer/pull/55
RouteRecognizer.ENCODE_AND_DECODE_PATH_SEGMENTS = true;
RouteRecognizer.Normalizer = {
    normalizeSegment: normalizeSegment, normalizePath: normalizePath, encodePathSegment: encodePathSegment
};
RouteRecognizer.prototype.map = map;

return RouteRecognizer;

})));

//# sourceMappingURL=route-recognizer.js.map

(function(e,r,t){var n,a,i,s;(function(){var e={},r={};n=function(r,t,n){e[r]={deps:t,callback:n}};s=i=a=function(t){if(r[t]){return r[t]}r[t]={};if(!e[t]){throw new Error("Could not find module "+t)}var n=e[t],i=n.deps,s=n.callback,o=[],l;for(var u=0,h=i.length;u<h;u++){if(i[u]==="exports"){o.push(l={})}else{o.push(a(d(i[u])))}}var f=s.apply(this,o);return r[t]=l||f;function d(e){if(e.charAt(0)!=="."){return e}var r=e.split("/");var n=t.split("/").slice(0,-1);for(var a=0,i=r.length;a<i;a++){var s=r[a];if(s===".."){n.pop()}else if(s==="."){continue}else{n.push(s)}}return n.join("/")}}})();n("router/handler-info",["./utils","rsvp","exports"],function(e,r,t){"use strict";var n=e.bind;var a=e.merge;var i=e.promiseLabel;var s=e.applyHook;var o=e.isPromise;var l=r.Promise;var u=Object.freeze({});function h(e){var r=e||{};this._handler=u;if(r.handler){var t=r.name;this.handlerPromise=l.resolve(r.handler);if(o(r.handler)){this.handlerPromise=this.handlerPromise.then(n(this,this.updateHandler));r.handler=undefined}else if(r.handler){r.handler._handlerName=t}}a(this,r);this.initialize(r)}h.prototype={name:null,getHandler:function(){},fetchHandler:function(){var e=this.getHandler(this.name);this.handlerPromise=l.resolve(e);if(o(e)){this.handlerPromise=this.handlerPromise.then(n(this,this.updateHandler))}else if(e){e._handlerName=this.name;return this.handler=e}return this.handler=undefined},_handlerPromise:undefined,params:null,context:null,factory:null,initialize:function(){},log:function(e,r){if(e.log){e.log(this.name+": "+r)}},promiseLabel:function(e){return i("'"+this.name+"' "+e)},getUnresolved:function(){return this},serialize:function(){return this.params||{}},updateHandler:function(e){e._handlerName=this.name;return this.handler=e},resolve:function(e,r){var t=n(this,this.checkForAbort,e),a=n(this,this.runBeforeModelHook,r),i=n(this,this.getModel,r),s=n(this,this.runAfterModelHook,r),o=n(this,this.becomeResolved,r),u=this;return l.resolve(this.handlerPromise,this.promiseLabel("Start handler")).then(function(e){return l.resolve(e).then(t,null,u.promiseLabel("Check for abort")).then(a,null,u.promiseLabel("Before model")).then(t,null,u.promiseLabel("Check if aborted during 'beforeModel' hook")).then(i,null,u.promiseLabel("Model")).then(t,null,u.promiseLabel("Check if aborted in 'model' hook")).then(s,null,u.promiseLabel("After model")).then(t,null,u.promiseLabel("Check if aborted in 'afterModel' hook")).then(o,null,u.promiseLabel("Become resolved"))},function(e){throw e})},runBeforeModelHook:function(e){if(e.trigger){e.trigger(true,"willResolveModel",e,this.handler)}return this.runSharedModelHook(e,"beforeModel",[])},runAfterModelHook:function(e,r){var t=this.name;this.stashResolvedModel(e,r);return this.runSharedModelHook(e,"afterModel",[r]).then(function(){return e.resolvedModels[t]},null,this.promiseLabel("Ignore fulfillment value and return model value"))},runSharedModelHook:function(e,r,t){this.log(e,"calling "+r+" hook");if(this.queryParams){t.push(this.queryParams)}t.push(e);var n=s(this.handler,r,t);if(n&&n.isTransition){n=null}return l.resolve(n,this.promiseLabel("Resolve value returned from one of the model hooks"))},getModel:null,checkForAbort:function(e,r){return l.resolve(e(),this.promiseLabel("Check for abort")).then(function(){return r},null,this.promiseLabel("Ignore fulfillment value and continue"))},stashResolvedModel:function(e,r){e.resolvedModels=e.resolvedModels||{};e.resolvedModels[this.name]=r},becomeResolved:function(e,r){var t=this.serialize(r);if(e){this.stashResolvedModel(e,r);e.params=e.params||{};e.params[this.name]=t}return this.factory("resolved",{context:r,name:this.name,handler:this.handler,params:t})},shouldSupercede:function(e){if(!e){return true}var r=e.context===this.context;return e.name!==this.name||this.hasOwnProperty("context")&&!r||this.hasOwnProperty("params")&&!f(this.params,e.params)}};Object.defineProperty(h.prototype,"handler",{get:function(){if(this._handler!==u){return this._handler}return this.fetchHandler()},set:function(e){return this._handler=e}});Object.defineProperty(h.prototype,"handlerPromise",{get:function(){if(this._handlerPromise){return this._handlerPromise}this.fetchHandler();return this._handlerPromise},set:function(e){return this._handlerPromise=e}});function f(e,r){if(!e^!r){return false}if(!e){return true}for(var t in e){if(e.hasOwnProperty(t)&&e[t]!==r[t]){return false}}return true}t["default"]=h});n("router/handler-info/factory",["./resolved-handler-info","./unresolved-handler-info-by-object","./unresolved-handler-info-by-param","exports"],function(e,r,t,n){"use strict";var a=e["default"];var i=r["default"];var s=t["default"];o.klasses={resolved:a,param:s,object:i};function o(e,r){var t=o.klasses[e],n=new t(r||{});n.factory=o;return n}n["default"]=o});n("router/handler-info/resolved-handler-info",["../handler-info","../utils","rsvp","exports"],function(e,r,t,n){"use strict";var a=e["default"];var i=r.subclass;var s=t.Promise;var o=i(a,{resolve:function(e,r){if(r&&r.resolvedModels){r.resolvedModels[this.name]=this.context}return s.resolve(this,this.promiseLabel("Resolve"))},getUnresolved:function(){return this.factory("param",{name:this.name,handler:this.handler,params:this.params})},isResolved:true});n["default"]=o});n("router/handler-info/unresolved-handler-info-by-object",["../handler-info","../utils","rsvp","exports"],function(e,r,t,n){"use strict";var a=e["default"];var i=r.subclass;var s=r.isParam;var o=t.Promise;var l=i(a,{getModel:function(e){this.log(e,this.name+": resolving provided model");return o.resolve(this.context)},initialize:function(e){this.names=e.names||[];this.context=e.context},serialize:function(e){var r=e||this.context,t=this.names,n=this.serializer||this.handler&&this.handler.serialize;var a={};if(s(r)){a[t[0]]=r;return a}if(n){return n(r,t)}if(t.length!==1){return}var i=t[0];if(/_id$/.test(i)){a[i]=r.id}else{a[i]=r}return a}});n["default"]=l});n("router/handler-info/unresolved-handler-info-by-param",["../handler-info","../utils","exports"],function(e,r,t){"use strict";var n=e["default"];var a=r.resolveHook;var i=r.merge;var s=r.subclass;var o=s(n,{initialize:function(e){this.params=e.params||{}},getModel:function(e){var r=this.params;if(e&&e.queryParams){r={};i(r,this.params);r.queryParams=e.queryParams}var t=this.handler;var n=a(t,"deserialize")||a(t,"model");return this.runSharedModelHook(e,n,[r])}});t["default"]=o});n("router/router",["route-recognizer","rsvp","./utils","./transition-state","./transition","./transition-aborted-error","./transition-intent/named-transition-intent","./transition-intent/url-transition-intent","exports"],function(e,r,t,n,a,i,s,o,l){"use strict";var u=e["default"];var h=r.Promise;var f=t.trigger;var d=t.log;var c=t.slice;var v=t.forEach;var m=t.merge;var p=t.extractQueryParams;var g=t.getChangelist;var y=t.promiseLabel;var b=t.callHook;var P=n["default"];var I=a.logAbort;var w=a.Transition;var x=i["default"];var T=s["default"];var H=o["default"];var q=Array.prototype.pop;function k(e){var r=e||{};this.getHandler=r.getHandler||this.getHandler;this.getSerializer=r.getSerializer||this.getSerializer;this.updateURL=r.updateURL||this.updateURL;this.replaceURL=r.replaceURL||this.replaceURL;this.didTransition=r.didTransition||this.didTransition;this.willTransition=r.willTransition||this.willTransition;this.delegate=r.delegate||this.delegate;this.triggerEvent=r.triggerEvent||this.triggerEvent;this.log=r.log||this.log;this.dslCallBacks=[];this.state=undefined;this.activeTransition=undefined;this._changedQueryParams=undefined;this.oldState=undefined;this.currentHandlerInfos=undefined;this.state=undefined;this.currentSequence=0;this.recognizer=new u;this.reset()}function L(e,r){var t=!!this.activeTransition;var n=t?this.activeTransition.state:this.state;var a;var i=e.applyToState(n,this.recognizer,this.getHandler,r,this.getSerializer);var s=g(n.queryParams,i.queryParams);if(O(i.handlerInfos,n.handlerInfos)){if(s){a=this.queryParamsTransition(s,t,n,i);if(a){a.queryParamsOnly=true;return a}}return this.activeTransition||new w(this)}if(r){R(this,i);return}a=new w(this,e,i,undefined,this.activeTransition);if(U(i.handlerInfos,n.handlerInfos)){a.queryParamsOnly=true}if(this.activeTransition){this.activeTransition.abort()}this.activeTransition=a;a.promise=a.promise.then(function(e){return C(a,e.state)},null,y("Settle transition promise when transition is finalized"));if(!t){N(this,i,a)}z(this,i,s);return a}k.prototype={map:function(e){this.recognizer.delegate=this.delegate;this.recognizer.map(e,function(e,r){for(var t=r.length-1,n=true;t>=0&&n;--t){var a=r[t];e.add(r,{as:a.handler});n=a.path==="/"||a.path===""||a.handler.slice(-6)===".index"}})},hasRoute:function(e){return this.recognizer.hasRoute(e)},getHandler:function(){},getSerializer:function(){},queryParamsTransition:function(e,r,t,n){var a=this;z(this,n,e);if(!r&&this.activeTransition){return this.activeTransition}else{var i=new w(this);i.queryParamsOnly=true;t.queryParams=_(this,n.handlerInfos,n.queryParams,i);i.promise=i.promise.then(function(e){M(i,t,true);if(a.didTransition){a.didTransition(a.currentHandlerInfos)}return e},null,y("Transition complete"));return i}},transitionByIntent:function(e){try{return L.apply(this,arguments)}catch(r){return new w(this,e,null,r)}},reset:function(){if(this.state){v(this.state.handlerInfos.slice().reverse(),function(e){var r=e.handler;b(r,"exit")})}this.oldState=undefined;this.state=new P;this.currentHandlerInfos=null},activeTransition:null,handleURL:function(e){var r=c.call(arguments);if(e.charAt(0)!=="/"){r[0]="/"+e}return E(this,r).method(null)},updateURL:function(){throw new Error("updateURL is not implemented")},replaceURL:function(e){this.updateURL(e)},transitionTo:function(){return E(this,arguments)},intermediateTransitionTo:function(){return E(this,arguments,true)},refresh:function(e){var r=this.activeTransition;var t=r?r.state:this.state;var n=t.handlerInfos;var a={};for(var i=0,s=n.length;i<s;++i){var o=n[i];a[o.name]=o.params||{}}d(this,"Starting a refresh transition");var l=new T({name:n[n.length-1].name,pivotHandler:e||n[0].handler,contexts:[],queryParams:this._changedQueryParams||t.queryParams||{}});var u=this.transitionByIntent(l,false);if(r&&r.urlMethod==="replace"){u.method(r.urlMethod)}return u},replaceWith:function(){return E(this,arguments).method("replace")},generate:function(e){var r=p(c.call(arguments,1)),t=r[0],n=r[1];var a=new T({name:e,contexts:t});var i=a.applyToState(this.state,this.recognizer,this.getHandler,null,this.getSerializer);var s={};for(var o=0,l=i.handlerInfos.length;o<l;++o){var u=i.handlerInfos[o];var h=u.serialize();m(s,h)}s.queryParams=n;return this.recognizer.generate(e,s)},applyIntent:function(e,r){var t=new T({name:e,contexts:r});var n=this.activeTransition&&this.activeTransition.state||this.state;return t.applyToState(n,this.recognizer,this.getHandler,null,this.getSerializer)},isActiveIntent:function(e,r,t,n){var a=n||this.state,i=a.handlerInfos,s,o;if(!i.length){return false}var l=i[i.length-1].name;var u=this.recognizer.handlersFor(l);var h=0;for(o=u.length;h<o;++h){s=i[h];if(s.name===e){break}}if(h===u.length){return false}var f=new P;f.handlerInfos=i.slice(0,h+1);u=u.slice(0,h+1);var d=new T({name:l,contexts:r});var c=d.applyToHandlers(f,u,this.getHandler,l,true,true,this.getSerializer);var v=O(c.handlerInfos,f.handlerInfos);if(!t||!v){return v}var p={};m(p,t);var y=a.queryParams;for(var b in y){if(y.hasOwnProperty(b)&&p.hasOwnProperty(b)){p[b]=y[b]}}return v&&!g(p,t)},isActive:function(e){var r=p(c.call(arguments,1));return this.isActiveIntent(e,r[0],r[1])},trigger:function(){var e=c.call(arguments);f(this,this.currentHandlerInfos,false,e)},log:null};function z(e,r,t){if(t){e._changedQueryParams=t.all;f(e,r.handlerInfos,true,["queryParamsDidChange",t.changed,t.all,t.removed]);e._changedQueryParams=null}}function R(e,r,t){var n=A(e.state,r);var a,i,s;for(a=0,i=n.exited.length;a<i;a++){s=n.exited[a].handler;delete s.context;b(s,"reset",true,t);b(s,"exit",t)}var o=e.oldState=e.state;e.state=r;var l=e.currentHandlerInfos=n.unchanged.slice();try{for(a=0,i=n.reset.length;a<i;a++){s=n.reset[a].handler;b(s,"reset",false,t)}for(a=0,i=n.updatedContext.length;a<i;a++){S(l,n.updatedContext[a],false,t)}for(a=0,i=n.entered.length;a<i;a++){S(l,n.entered[a],true,t)}}catch(u){e.state=o;e.currentHandlerInfos=o.handlerInfos;throw u}e.state.queryParams=_(e,l,r.queryParams,t)}function S(e,r,t,n){var a=r.handler,i=r.context;function s(a){if(t){b(a,"enter",n)}if(n&&n.isAborted){throw new x}a.context=i;b(a,"contextDidChange");b(a,"setup",i,n);if(n&&n.isAborted){throw new x}e.push(r)}if(!a){r.handlerPromise=r.handlerPromise.then(s)}else{s(a)}return true}function A(e,r){var t=e.handlerInfos;var n=r.handlerInfos;var a={updatedContext:[],exited:[],entered:[],unchanged:[],reset:undefined};var i,s=false,o,l;for(o=0,l=n.length;o<l;o++){var u=t[o],h=n[o];if(!u||u.handler!==h.handler){i=true}if(i){a.entered.push(h);if(u){a.exited.unshift(u)}}else if(s||u.context!==h.context){s=true;a.updatedContext.push(h)}else{a.unchanged.push(u)}}for(o=n.length,l=t.length;o<l;o++){a.exited.unshift(t[o])}a.reset=a.updatedContext.slice();a.reset.reverse();return a}function M(e,r){var t=e.urlMethod;if(!t){return}var n=e.router,a=r.handlerInfos,i=a[a.length-1].name,s={};for(var o=a.length-1;o>=0;--o){var l=a[o];m(s,l.params);if(l.handler.inaccessibleByURL){t=null}}if(t){s.queryParams=e._visibleQueryParams||r.queryParams;var u=n.recognizer.generate(i,s);var h=e.isCausedByInitialTransition;var f=t==="replace"&&!e.isCausedByAbortingTransition;var d=e.queryParamsOnly&&t==="replace";if(h||f||d){n.replaceURL(u)}else{n.updateURL(u)}}}function C(e,r){try{d(e.router,e.sequence,"Resolved all models on destination route; finalizing transition.");var t=e.router,n=r.handlerInfos;R(t,r,e);if(e.isAborted){t.state.handlerInfos=t.currentHandlerInfos;return h.reject(I(e))}M(e,r,e.intent.url);e.isActive=false;t.activeTransition=null;f(t,t.currentHandlerInfos,true,["didTransition"]);if(t.didTransition){t.didTransition(t.currentHandlerInfos)}d(t,e.sequence,"TRANSITION COMPLETE.");return n[n.length-1].handler}catch(a){if(!((a instanceof x))){var i=e.state.handlerInfos;e.trigger(true,"error",a,e,i[i.length-1].handler);e.abort()}throw a}}function E(e,r,t){var n=r[0]||"/";var a=r[r.length-1];var i={};if(a&&a.hasOwnProperty("queryParams")){i=q.call(r).queryParams}var s;if(r.length===0){d(e,"Updating query params");var o=e.state.handlerInfos;s=new T({name:o[o.length-1].name,contexts:[],queryParams:i})}else if(n.charAt(0)==="/"){d(e,"Attempting URL transition to "+n);s=new H({url:n})}else{d(e,"Attempting transition to "+n);s=new T({name:r[0],contexts:c.call(r,1),queryParams:i})}return e.transitionByIntent(s,t)}function O(e,r){if(e.length!==r.length){return false}for(var t=0,n=e.length;t<n;++t){if(e[t]!==r[t]){return false}}return true}function U(e,r){if(e.length!==r.length){return false}for(var t=0,n=e.length;t<n;++t){if(e[t].name!==r[t].name){return false}if(!j(e[t].params,r[t].params)){return false}}return true}function j(e,r){if(!e&&!r){return true}else if(!e&&!!r||!!e&&!r){return false}var t=Object.keys(e);var n=Object.keys(r);if(t.length!==n.length){return false}for(var a=0,i=t.length;a<i;++a){var s=t[a];if(e[s]!==r[s]){return false}}return true}function _(e,r,t,n){for(var a in t){if(t.hasOwnProperty(a)&&t[a]===null){delete t[a]}}var i=[];f(e,r,true,["finalizeQueryParamChange",t,i,n]);if(n){n._visibleQueryParams={}}var s={};for(var o=0,l=i.length;o<l;++o){var u=i[o];s[u.key]=u.value;if(n&&u.visible!==false){n._visibleQueryParams[u.key]=u.value}}return s}function N(e,r,t){var n=e.state.handlerInfos,a=[],i=null,s,o,l,u,h,d;u=n.length;for(l=0;l<u;l++){h=n[l];d=r.handlerInfos[l];if(!d||h.name!==d.name){i=l;break}if(!d.isResolved){a.push(h)}}if(i!==null){s=n.slice(i,u);o=function(e){for(var r=0,t=s.length;r<t;r++){if(s[r].name===e){return true}}return false}}f(e,n,true,["willTransition",t]);if(e.willTransition){e.willTransition(n,r.handlerInfos,t)}}l["default"]=k});n("router/transition-aborted-error",["./utils","exports"],function(e,r){"use strict";var t=e.oCreate;function n(e){if(!(this instanceof n)){return new n(e)}var r=Error.call(this,e);if(Error.captureStackTrace){Error.captureStackTrace(this,n)}else{this.stack=r.stack}this.description=r.description;this.fileName=r.fileName;this.lineNumber=r.lineNumber;this.message=r.message||"TransitionAborted";this.name="TransitionAborted";this.number=r.number;this.code=r.code}n.prototype=t(Error.prototype);r["default"]=n});n("router/transition-intent",["exports"],function(e){"use strict";function r(e){this.initialize(e);this.data=this.data||{}}r.prototype={initialize:null,applyToState:null};e["default"]=r});n("router/transition-intent/named-transition-intent",["../transition-intent","../transition-state","../handler-info/factory","../utils","exports"],function(e,r,t,n,a){"use strict";var i=e["default"];var s=r["default"];var o=t["default"];var l=n.isParam;var u=n.extractQueryParams;var h=n.merge;var f=n.subclass;a["default"]=f(i,{name:null,pivotHandler:null,contexts:null,queryParams:null,initialize:function(e){this.name=e.name;this.pivotHandler=e.pivotHandler;this.contexts=e.contexts||[];this.queryParams=e.queryParams},applyToState:function(e,r,t,n,a){var i=u([this.name].concat(this.contexts)),s=i[0],o=r.handlersFor(s[0]);var l=o[o.length-1].handler;return this.applyToHandlers(e,o,t,l,n,null,a)},applyToHandlers:function(e,r,t,n,a,i,o){var l,u;var f=new s;var d=this.contexts.slice(0);var c=r.length;if(this.pivotHandler){for(l=0,u=r.length;l<u;++l){if(r[l].handler===this.pivotHandler._handlerName){c=l;break}}}for(l=r.length-1;l>=0;--l){var v=r[l];var m=v.handler;var p=e.handlerInfos[l];var g=null;if(v.names.length>0){if(l>=c){g=this.createParamHandlerInfo(m,t,v.names,d,p)}else{var y=o(m);g=this.getHandlerInfoForDynamicSegment(m,t,v.names,d,p,n,l,y)}}else{g=this.createParamHandlerInfo(m,t,v.names,d,p)}if(i){g=g.becomeResolved(null,g.context);var b=p&&p.context;if(v.names.length>0&&g.context===b){g.params=p&&p.params}g.context=b}var P=p;if(l>=c||g.shouldSupercede(p)){c=Math.min(l,c);P=g}if(a&&!i){P=P.becomeResolved(null,P.context)}f.handlerInfos.unshift(P)}if(d.length>0){throw new Error("More context objects were passed than there are dynamic segments for the route: "+n)}if(!a){this.invalidateChildren(f.handlerInfos,c)}h(f.queryParams,this.queryParams||{});return f},invalidateChildren:function(e,r){for(var t=r,n=e.length;t<n;++t){var a=e[t];e[t]=a.getUnresolved()}},getHandlerInfoForDynamicSegment:function(e,r,t,n,a,i,s,u){var h;if(n.length>0){h=n[n.length-1];if(l(h)){return this.createParamHandlerInfo(e,r,t,n,a)}else{n.pop()}}else if(a&&a.name===e){return a}else{if(this.preTransitionState){var f=this.preTransitionState.handlerInfos[s];h=f&&f.context}else{return a}}return o("object",{name:e,getHandler:r,serializer:u,context:h,names:t})},createParamHandlerInfo:function(e,r,t,n,a){var i={};var s=t.length;while(s--){var u=a&&e===a.name&&a.params||{};var h=n[n.length-1];var f=t[s];if(l(h)){i[f]=""+n.pop()}else{if(u.hasOwnProperty(f)){i[f]=u[f]}else{throw new Error("You didn't provide enough string/numeric parameters to satisfy all of the dynamic segments for route "+e)}}}return o("param",{name:e,getHandler:r,params:i})}})});n("router/transition-intent/url-transition-intent",["../transition-intent","../transition-state","../handler-info/factory","../utils","../unrecognized-url-error","exports"],function(e,r,t,n,a,i){"use strict";var s=e["default"];var o=r["default"];var l=t["default"];var u=n.merge;var h=n.subclass;var f=a["default"];i["default"]=h(s,{url:null,initialize:function(e){this.url=e.url},applyToState:function(e,r,t){var n=new o;var a=r.recognize(this.url),i,s;if(!a){throw new f(this.url)}var h=false;var d=this.url;function c(e){if(e&&e.inaccessibleByURL){throw new f(d)}return e}for(i=0,s=a.length;i<s;++i){var v=a[i];var m=v.handler;var p=l("param",{name:m,getHandler:t,params:v.params});var g=p.handler;if(g){c(g)}else{p.handlerPromise=p.handlerPromise.then(c)}var y=e.handlerInfos[i];if(h||p.shouldSupercede(y)){h=true;n.handlerInfos[i]=p}else{n.handlerInfos[i]=y}}u(n.queryParams,a.queryParams);return n}})});n("router/transition-state",["./utils","rsvp","exports"],function(e,r,t){"use strict";var n=e.forEach;var a=e.promiseLabel;var i=e.callHook;var s=r.Promise;function o(){this.handlerInfos=[];this.queryParams={};this.params={}}o.prototype={promiseLabel:function(e){var r="";n(this.handlerInfos,function(e){if(r!==""){r+="."}r+=e.name});return a("'"+r+"': "+e)},resolve:function(e,r){var t=this.params;n(this.handlerInfos,function(e){t[e.name]=e.params||{}});r=r||{};r.resolveIndex=0;var a=this;var o=false;return s.resolve(null,this.promiseLabel("Start transition")).then(f,null,this.promiseLabel("Resolve handler"))["catch"](u,this.promiseLabel("Handle error"));function l(){return s.resolve(e(),a.promiseLabel("Check if should continue"))["catch"](function(e){o=true;return s.reject(e)},a.promiseLabel("Handle abort"))}function u(e){var t=a.handlerInfos;var n=r.resolveIndex>=t.length?t.length-1:r.resolveIndex;return s.reject({error:e,handlerWithError:a.handlerInfos[n].handler,wasAborted:o,state:a})}function h(e){var t=a.handlerInfos[r.resolveIndex].isResolved;a.handlerInfos[r.resolveIndex++]=e;if(!t){var n=e.handler;i(n,"redirect",e.context,r)}return l().then(f,null,a.promiseLabel("Resolve handler"))}function f(){if(r.resolveIndex===a.handlerInfos.length){return{error:null,state:a}}var e=a.handlerInfos[r.resolveIndex];return e.resolve(l,r).then(h,null,a.promiseLabel("Proceed"))}}};t["default"]=o});n("router/transition",["rsvp","./utils","./transition-aborted-error","exports"],function(e,r,t,n){"use strict";var a=e.Promise;var i=r.trigger;var s=r.slice;var o=r.log;var l=r.promiseLabel;var u=t["default"];function h(e,r,t,n,i){var s=this;this.state=t||e.state;this.intent=r;this.router=e;this.data=this.intent&&this.intent.data||{};this.resolvedModels={};this.queryParams={};this.promise=undefined;this.error=undefined;this.params=undefined;this.handlerInfos=undefined;this.targetName=undefined;this.pivotHandler=undefined;this.sequence=undefined;this.isAborted=false;this.isActive=true;if(n){this.promise=a.reject(n);this.error=n;return}this.isCausedByAbortingTransition=!!i;this.isCausedByInitialTransition=i&&(i.isCausedByInitialTransition||i.sequence===0);if(t){this.params=t.params;this.queryParams=t.queryParams;this.handlerInfos=t.handlerInfos;var o=t.handlerInfos.length;if(o){this.targetName=t.handlerInfos[o-1].name}for(var u=0;u<o;++u){var h=t.handlerInfos[u];if(!h.isResolved){break}this.pivotHandler=h.handler}this.sequence=e.currentSequence++;this.promise=t.resolve(d,this)["catch"](f(s),l("Handle Abort"))}else{this.promise=a.resolve(this.state);this.params={}}function d(){if(s.isAborted){return a.reject(undefined,l("Transition aborted - reject"))}}}function f(e){return function(r){if(r.wasAborted||e.isAborted){return a.reject(d(e))}else{e.trigger("error",r.error,e,r.handlerWithError);e.abort();return a.reject(r.error)}}}h.prototype={targetName:null,urlMethod:"update",intent:null,pivotHandler:null,resolveIndex:0,resolvedModels:null,state:null,queryParamsOnly:false,isTransition:true,isExiting:function(e){var r=this.handlerInfos;for(var t=0,n=r.length;t<n;++t){var a=r[t];if(a.name===e||a.handler===e){return false}}return true},promise:null,data:null,then:function(e,r,t){return this.promise.then(e,r,t)},"catch":function(e,r){return this.promise["catch"](e,r)},"finally":function(e,r){return this.promise["finally"](e,r)},abort:function(){if(this.isAborted){return this}o(this.router,this.sequence,this.targetName+": transition was aborted");this.intent.preTransitionState=this.router.state;this.isAborted=true;this.isActive=false;this.router.activeTransition=null;return this},retry:function(){this.abort();var e=this.router.transitionByIntent(this.intent,false);if(this.urlMethod!==null){e.method(this.urlMethod)}return e},method:function(e){this.urlMethod=e;return this},trigger:function(e){var r=s.call(arguments);if(typeof e==="boolean"){r.shift()}else{e=false}i(this.router,this.state.handlerInfos.slice(0,this.resolveIndex+1),e,r)},followRedirects:function(){var e=this.router;return this.promise["catch"](function(r){if(e.activeTransition){return e.activeTransition.followRedirects()}return a.reject(r)})},toString:function(){return"Transition (sequence "+this.sequence+")"},log:function(e){o(this.router,this.sequence,e)}};h.prototype.send=h.prototype.trigger;function d(e){o(e.router,e.sequence,"detected abort.");return new u}n.Transition=h;n.logAbort=d;n.TransitionAbortedError=u});n("router/unrecognized-url-error",["./utils","exports"],function(e,r){"use strict";var t=e.oCreate;function n(e){if(!(this instanceof n)){return new n(e)}var r=Error.call(this,e);if(Error.captureStackTrace){Error.captureStackTrace(this,n)}else{this.stack=r.stack}this.description=r.description;this.fileName=r.fileName;this.lineNumber=r.lineNumber;this.message=r.message||"UnrecognizedURL";this.name="UnrecognizedURLError";this.number=r.number;this.code=r.code}n.prototype=t(Error.prototype);r["default"]=n});n("router/utils",["exports"],function(e){"use strict";var r=Array.prototype.slice;var t;if(!Array.isArray){t=function(e){return Object.prototype.toString.call(e)==="[object Array]"}}else{t=Array.isArray}var n=t;e.isArray=n;function a(e){return(typeof e==="object"&&e!==null||typeof e==="function")&&typeof e.then==="function"}e.isPromise=a;function i(e,r){for(var t in r){if(r.hasOwnProperty(t)){e[t]=r[t]}}}var s=Object.create||function(e){function r(){}r.prototype=e;return new r};e.oCreate=s;function o(e){var t=e&&e.length,n,a;if(t&&t>0&&e[t-1]&&e[t-1].hasOwnProperty("queryParams")){a=e[t-1].queryParams;n=r.call(e,0,t-1);return[n,a]}else{return[e,null]}}e.extractQueryParams=o;function l(e){for(var r in e){if(typeof e[r]==="number"){e[r]=""+e[r]}else if(n(e[r])){for(var t=0,a=e[r].length;t<a;t++){e[r][t]=""+e[r][t]}}}}function u(e,r,t){if(!e.log){return}if(arguments.length===3){e.log("Transition #"+r+": "+t)}else{t=r;e.log(t)}}e.log=u;function h(e,t){var n=arguments;return function(a){var i=r.call(n,2);i.push(a);return t.apply(e,i)}}e.bind=h;function f(e){return typeof e==="string"||e instanceof String||typeof e==="number"||e instanceof Number}function d(e,r){for(var t=0,n=e.length;t<n&&false!==r(e[t]);t++){}}e.forEach=d;function c(e,r,t,n){if(e.triggerEvent){e.triggerEvent(r,t,n);return}var a=n.shift();if(!r){if(t){return}throw new Error("Could not trigger event '"+a+"'. There are no active handlers")}var i=false;function s(e,r,t){t.events[e].apply(t,r)}for(var o=r.length-1;o>=0;o--){var l=r[o],u=l.handler;if(!u){l.handlerPromise.then(h(null,s,a,n));continue}if(u.events&&u.events[a]){if(u.events[a].apply(u,n)===true){i=true}else{return}}}if(a==="error"&&n[0].name==="UnrecognizedURLError"){throw n[0]}else if(!i&&!t){throw new Error("Nothing handled the event '"+a+"'.")}}e.trigger=c;function v(e,r){var t;var a={all:{},changed:{},removed:{}};i(a.all,r);var s=false;l(e);l(r);for(t in e){if(e.hasOwnProperty(t)){if(!r.hasOwnProperty(t)){s=true;a.removed[t]=e[t]}}}for(t in r){if(r.hasOwnProperty(t)){if(n(e[t])&&n(r[t])){if(e[t].length!==r[t].length){a.changed[t]=r[t];s=true}else{for(var o=0,u=e[t].length;o<u;o++){if(e[t][o]!==r[t][o]){a.changed[t]=r[t];s=true}}}}else{if(e[t]!==r[t]){a.changed[t]=r[t];s=true}}}}return s&&a}e.getChangelist=v;function m(e){return"Router: "+e}e.promiseLabel=m;function p(e,r){function t(r){e.call(this,r||{})}t.prototype=s(e.prototype);i(t.prototype,r);return t}e.subclass=p;function g(e,r){if(!e){return}var t="_"+r;return e[t]&&t||e[r]&&r}function y(e,r,t,n){var a=g(e,r);return a&&e[a].call(e,t,n)}function b(e,r,t){var n=g(e,r);if(n){if(t.length===0){return e[n].call(e)}else if(t.length===1){return e[n].call(e,t[0])}else if(t.length===2){return e[n].call(e,t[0],t[1])}else{return e[n].apply(e,t)}}}e.merge=i;e.slice=r;e.isParam=f;e.coerceQueryParamsToString=l;e.callHook=y;e.resolveHook=g;e.applyHook=b});n("router",["./router/router","./router/transition","exports"],function(e,r,t){"use strict";var n=e["default"];var a=r.Transition;t["default"]=n;t.Transition=a});n("route-recognizer",[],function(){return{"default":t}});n("rsvp",[],function(){return r});n("rsvp/promise",[],function(){return{"default":r.Promise}});window.Router=a("router")})(window,window.RSVP,window.RouteRecognizer);
/*!
 * Hasher <http://github.com/millermedeiros/hasher>
 * @author Miller Medeiros
 * @version 1.2.0 (2013/11/11 03:18 PM)
 * Released under the MIT License
 */
(function(){var a=function(b){var c=(function(k){var p=25,r=k.document,n=k.history,x=b.Signal,f,v,m,F,d,D,t=/#(.*)$/,j=/(\?.*)|(\#.*)/,g=/^\#/,i=(!+"\v1"),B=("onhashchange" in k)&&r.documentMode!==7,e=i&&!B,s=(location.protocol==="file:");function o(G){return String(G||"").replace(/\W/g,"\\$&")}function u(H){if(!H){return""}var G=new RegExp("^"+o(f.prependHash)+"|"+o(f.appendHash)+"$","g");return H.replace(G,"")}function E(){var G=t.exec(f.getURL());var I=(G&&G[1])||"";try{return f.raw?I:decodeURIComponent(I)}catch(H){return I}}function A(){return(d)?d.contentWindow.frameHash:null}function z(){d=r.createElement("iframe");d.src="about:blank";d.style.display="none";r.body.appendChild(d)}function h(){if(d&&v!==A()){var G=d.contentWindow.document;G.open();G.write("<html><head><title>"+r.title+'</title><script type="text/javascript">var frameHash="'+v+'";<\/script></head><body>&nbsp;</body></html>');G.close()}}function l(G,H){if(v!==G){var I=v;v=G;if(e){if(!H){h()}else{d.contentWindow.frameHash=G}}f.changed.dispatch(u(G),u(I))}}if(e){D=function(){var H=E(),G=A();if(G!==v&&G!==H){f.setHash(u(G))}else{if(H!==v){l(H)}}}}else{D=function(){var G=E();if(G!==v){l(G)}}}function C(I,G,H){if(I.addEventListener){I.addEventListener(G,H,false)}else{if(I.attachEvent){I.attachEvent("on"+G,H)}}}function y(I,G,H){if(I.removeEventListener){I.removeEventListener(G,H,false)}else{if(I.detachEvent){I.detachEvent("on"+G,H)}}}function q(H){H=Array.prototype.slice.call(arguments);var G=H.join(f.separator);G=G?f.prependHash+G.replace(g,"")+f.appendHash:G;return G}function w(G){G=encodeURI(G);if(i&&s){G=G.replace(/\?/,"%3F")}return G}f={VERSION:"1.2.0",raw:false,appendHash:"",prependHash:"/",separator:"/",changed:new x(),stopped:new x(),initialized:new x(),init:function(){if(F){return}v=E();if(B){C(k,"hashchange",D)}else{if(e){if(!d){z()}h()}m=setInterval(D,p)}F=true;f.initialized.dispatch(u(v))},stop:function(){if(!F){return}if(B){y(k,"hashchange",D)}else{clearInterval(m);m=null}F=false;f.stopped.dispatch(u(v))},isActive:function(){return F},getURL:function(){return k.location.href},getBaseURL:function(){return f.getURL().replace(j,"")},setHash:function(G){G=q.apply(null,arguments);if(G!==v){l(G);if(G===v){if(!f.raw){G=w(G)}k.location.hash="#"+G}}},replaceHash:function(G){G=q.apply(null,arguments);if(G!==v){l(G,true);if(G===v){if(!f.raw){G=w(G)}k.location.replace("#"+G)}}},getHash:function(){return u(v)},getHashAsArray:function(){return f.getHash().split(f.separator)},dispose:function(){f.stop();f.initialized.dispose();f.stopped.dispose();f.changed.dispose();d=f=k.hasher=null},toString:function(){return'[hasher version="'+f.VERSION+'" hash="'+f.getHash()+'"]'}};f.initialized.memorize=true;return f}(window));return c};if(typeof define==="function"&&define.amd){define(["signals"],a)}else{if(typeof exports==="object"){module.exports=a(require("signals"))}else{window.hasher=a(window.signals)}}}());
"use strict";
var AppRouter = function() {
    var App = {
        name: "",
        author: "",
        year: "",
        url: ""
    };
    var DefaultOptions = {};
    var SessionData = "";
    var stateExtras = {};
    var beforeChange;
    var afterChange;
    var onChange;
    var stateParams;
    var states = [];
    var router;
    var handlers = {};
    var insertAfter;
    var insertBefore;
    var shouldClear = false;
    var handleViewError;
    var hash;

    function handleChanges(newHash, oldHash) {
        stateExtras = {};
        var state = getState(states, newHash.toLowerCase().split('/'));
        if (isFunction(beforeChange)) {
            beforeChange(newHash.toLowerCase(), state).then(function(e) {
                if (e)
                    goToNext(newHash, oldHash)
            });
        } else
            goToNext(newHash, oldHash)
    }

    function goToNext(newHash, oldHash) {
        router.handleURL(newHash.toLowerCase());
        hash = newHash;
        if (newHash.toLowerCase().indexOf(oldHash ? oldHash.toLowerCase() : oldHash) == -1) {
            shouldClear = true;
        }
    }

    function isFunction(functionToCheck) {
        var getType = {};
        return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
    }

    function getState(current, hash) {
        var state = "";
        for (var key in current) {
            if (key != "options" && current[key].options !== undefined && current[key].options.url.toLowerCase().substring(1) == hash[0]) {
                if (hash.length > 1) {
                    hash = hash.splice(1, 1);
                    state = getState(current[key], hash);
                } else {
                    state = {
                        state: current[key].options.state,
                        params: current[key].options.params
                    };
                }
            }
        }

        if (state !== "")
            return state;
    }

    function pushState(pushTo, state, options) {
        if (state.length == 1) {
            if (pushTo[state[0]] == undefined)
                pushTo[state[0]] = [];
            options.state = state[0];
            pushTo[state[0]].options = options;
        } else {
            var prev = state.slice(0, 1);
            if (pushTo[prev] == undefined)
                pushTo[prev] = [];
            state.splice(0, 1);
            pushState(pushTo[prev], state, options);
        }
    }

    function goToState(current, state, url, params) {
        current = current[state[0]];
        if (current != undefined)
            url += current.options.url;
        else {
            console.error("Invalid state.")
            return;
        }
        if (state.length == 1) {
            for (var key in params) {
                if (params.hasOwnProperty(key)) {
                    url = url.replace(":" + key, params[key]);
                }
            }
            window.location.hash = url;
        } else {
            state = state.splice(1);
            
            goToState(current, state, url, params);
        }
    }

    function setMapping() {
        router.map(function(match) {
            match("/").to("defaultState");
            for (var key in states) {
                recursiveMapping(match, states[key], key, 0);
            }
            match("/*").to("notFound");
        });
    }

    function recursiveMapping(match, state, key, level) {
        if (Object.keys(state).length == 1) {
            match(state.options.url.toLowerCase() + '/').to(key);
            match(state.options.url.toLowerCase()).to(key);
            match("/*").to("notFound");
        } else {
            match(state.options.url.toLowerCase() + '/').to(key);
            match(state.options.url.toLowerCase()).to(key, function(match) {
                for (var key in state) {
                    if (key != "options")
                        recursiveMapping(match, state[key], key, level + 1);
                }
            });
        }
        setHandlers(key, state.options, level);
    }

    function setHandlers(key, options, level) {
        handlers[key] = {
            model: function() {
                var viewUrl = options.viewUrl;
                var location = window.location.hash.split('/');
                    var url = options.url.split('/');
                    $.each(url, function (index, value) { 
                        if(value.indexOf(':') >= 0)
                            viewUrl = viewUrl.replace(value, location[index]);
                    });
                return $.get(viewUrl).fail(handleViewError);
            },
            setup: function(template) {
                currentLevel = level;
                options = $.extend(true, options, DefaultOptions);
                var oldParams = AppRouter.getStateParams();

                stateParams = {
                    title: options.title,
                    state: options.state,
                    params: options.params,
                    controller: options.Controller
                };
                var container = $("body").find("[content-view]");
                container = $(container[level]);
                if (oldParams && oldParams.controller && typeof window[oldParams.controller].destroy === "function")
                    window[oldParams.controller].destroy();
                container.find('*').off();
                if (options.dependencies.css != undefined)
                    loaddependencies(options.dependencies);
                container.html(template);
                if(isFunction(onChange))
                    onChange();
                if (typeof window[options.Controller].init === "function")
                    window[options.Controller].init();
                else
                    console.warn("Controller init function not set.");
                if (container.attr('animated'))
                    container.animateCss('fadeInUp');
                if (isFunction(afterChange)) {
                    afterChange(hash);
                }
            }
        };
    }

    function loaddependencies(files) {
        $.post('/admin/cacheBuster', {
            files: files
        }, function(e) {
            dependecies(e, files.options);
        });
    }

    function dependecies(bundle, options) {
        if (shouldClear) {
            $(insertBefore).nextUntil(insertAfter).remove();
            shouldClear = false;
        }
        if (options != undefined && options.insertAfter != undefined && options.insertBefore != undefined) {
            insertAfter = options.insertAfter;
            insertBefore = options.insertBefore;
        } else {
            console.error("insertAfter and insertBefore must be set.");
            return;
        }
        $.each(bundle, function(i, dep) {
            var el;
            var version = dep.time ? '?v=' + dep.time : '';
            el = document.createElement('link');
            el.rel = 'stylesheet';
            el.href = dep.url + version;
            $(insertAfter).before(el);
        });
    }

    return {
        /**
         * Set app info
         * 
         * @param {Object} data
         * @param {string} data.name App name
         * @param {Object} data.author App creator
         * @param {string} data.year Year of creation
         * @param {string} data.url Url to the creator´s web page
         */
        setApp: function(data) {
            App = data;
            return this;
        },
        /**
         * Set default state options
         * 
         * @param {Object} options
         * @param {Object} options.dependencies
         */
        setDefaultOptions: function(options) {
            DefaultOptions = options;
            return this;
        },
        /**
         * Get default state options
         * 
         */
        getDefaultOptions: function() {
            return DefaultOptions;
        },
        /**
         * Set default state
         * 
         * @param {string} state state name
         */
        setDefaultState: function(state) {
            handlers.defaultState = {
                beforeModel: function() {
                    throw "";
                    return RSVP.reject("");
                },
                events: {
                    error: function(error, transition) {
                        AppRouter.goToState(state);
                    }
                }
            };
            return this;
        },
        /**
         * Set the function that will be called every time the state changes
         * 
         * @param {function} fn function with the state change logic
         */
        setBeforeChange: function(fn) {
            beforeChange = fn;
            return this;
        },
        setAfterChange: function(fn) {
            afterChange = fn;
            return this;
        },
        setOnChange: function(fn) {
            onChange = fn;
            return this;
        },
        /**
         * Set state temporal values (resets on state change)
         * 
         * @param {Object} extras json with extra data
         */
        setStateExtras: function(extras) {
            stateExtras = extras;
        },
        /**
         * Get state temporal values (resets on state change)
         * 
         */
        getStateExtras: function() {
            return stateExtras;
        },
        /**
         * Set the not found state options
         * 
         * @param {Object} options
         * @param {string} options.url The url sub part of this state
         * @param {Object} options.params custom parameters that may be required for the state logic
         * @param {string} options.title The title the uses will see
         * @param {string} options.viewUrl Url to the template view
         * @param {string} options.Module The controller module name defined in you controller .js file
         * @param {Object} options.dependencies javascript and css dependencies of the controller 
         */
        setNotFound: function(options) {
            handlers.notFound = {
                setup: function(posts) {
                    $.get(options.viewUrl, function(template) {
                        var container = $("body").find("[content-view]").last();
                        container.find('*').off();
                        container.html(template);
                        if (typeof window[options.Controller].init === "function")
                            window[options.Controller].init();
                        else
                            console.warn("Controller init function not set.");
                        if (container.attr('animated'))
                            container.animateCss('fadeInUp');
                        if (options.dependencies.css != undefined)
                            loaddependencies(options.dependencies);

                        Common.unBlockPage();
                    }).fail(handleViewError);
                }
            };
            return this;
        },
        /**
         * Push a new state to the app
         * 
         * @param {string} state state name hierarchy is defined with '.' as separator
         * @param {Object} options
         * @param {string} options.url The url sub part of this state
         * @param {Object} options.params custom parameters that may be required for the state logic
         * @param {string} options.title The title the uses will see
         * @param {string} options.viewUrl Url to the template view
         * @param {string} options.Module The controller module name defined in you controller .js file
         * @param {Object} options.dependencies javascript and css dependencies of the controller 
         */
        state: function(state, options) {
            var parts = state.split('.');
            pushState(states, parts, options);
            return this;
        },
        /**
         * Go to the specified state
         * 
         * @param {string} state 
         */
        goToState: function(state, params) {
            if (state != 'notFound') {
                var parts = state.split('.');
                goToState(states, parts, "#", params);
            } else router.transitionTo(state);
        },
        /**
         * Returns the App info
         * 
         * @returns 
         */
        getApp: function() {
            return App
        },
        /**
         * Returns the accesble parameters of the current state
         * 
         */
        getStateParams: function() {
            return stateParams
        },
        /**
         * Getter for the session data that can be shared between states
         * 
         */
        getSessionData: function() {
            return SessionData;
        },
        /**
         * Setter for the session data that can be shared between states
         * 
         * @param {any} data 
         */
        setSessionData: function(data) {
            SessionData = data;
        },
        setHandleViewError: function(handler) {
            handleViewError = handler;
            return this;
        },
        /**
         * Initialize the AppRouter, call after everything has been setup
         * 
         */
        init: function() {
            router = new Router["default"]();
            router.getHandler = function(name) {
                return handlers[name];
            };
            setMapping();
            hasher.changed.add(handleChanges);
            hasher.initialized.add(handleChanges);
            hasher.init();
        }
    }
}();
"use strict;"
$(document).ready(function() {
    AppRouter
        .setDefaultOptions({
            dependencies: {
                options: {
                    insertAfter: "#plugins_after",
                    insertBefore: "#plugins_before"
                }
            }
        })
        .setHandleViewError(function() {
            $.get('/admin/checksession')
                .done(function(e) {
                    if(!e.auth)
                        window.location = '/';
                })
                .fail(function() {
                    window.location = '/';
                });
        })
        .setNotFound({
            viewUrl: '/admin/notfound'
        })
        .setBeforeChange(function() {
            Common.blockPage();
            var d1 = $.Deferred();
            Common.destroyPortlets();
            $("body").removeClass('hide');
            d1.resolve(true);
            return d1;
        })
        .setAfterChange(function(hash){
            Common.handleChanges(hash);
            Common.activeMenu();
            Common.unBlockPage();
        })
        .setOnChange(function() {
            Common.initPortlets();
        })
        .state('permission', {
            url: '/Permisos',
            title: 'Permisos',
            viewUrl: '/admin/permission',
            Controller: 'Permisos',
            dependencies: {
                css: [
                    "/css/permisos.css"
                ]
            }
        })
        .state('role', {
            url: '/Roles',
            title: 'Roles',
            viewUrl: '/admin/role',
            Controller: 'Roles',
            dependencies: {
                css: [
                    "/css/roles.css"
                ]
            }
        })
        .state('users', {
            url: '/Usuarios',
            title: 'Usuarios',
            viewUrl: '/admin/user',
            Controller: 'Users'
        })
        .state('dependency', {
            url: '/Dependencias',
            title: 'Dependencias',
            viewUrl: '/admin/dependency',
            Controller: 'Dependencies'
        })
        .state('department', {
            url: '/Departamentos',
            title: 'Departamentos',
            viewUrl: '/admin/department',
            Controller: 'Departments'
        });
});