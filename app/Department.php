<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable = [
        'id', 'name', 'attendant', 'dependency_id', 'active'
    ];

    public function dependency()
    {
        return $this->belongsTo('App\Dependency');
    }
}
